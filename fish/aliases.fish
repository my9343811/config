
alias gco="git checkout"
alias gb="git branch"
alias ga="git add ."
alias gc="git commit -m"
alias gi="git init"
alias gpo="git push -u origin"
alias gs="git status"